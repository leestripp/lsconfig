#include "lsConfig.h"

int main()
{
	lsConfig config;
	
	if( config.load( "test1.config" ) )
	{
		config.print();
	} else
	{
		config.add_entry( "name1", "Joe Blogs" );
		config.add_entry( "name2", "Jane Doe" );
		config.add_entry( "name3", "Bob Smith" );
		config.print();
		
		// save
		config.save();
	}
	
	return 0;
}
