#ifndef LSCONFIG_H
#define LSCONFIG_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <filesystem>
#include <fstream>

using namespace std;

class lsConfig
{
public:
	lsConfig();
	
	bool load( const string& full_path );
	void save();
	void add_entry( const string& key, const string& value );
	string get_value( const string& key );
	void print();
	
	// utils
	vector<string> split( string str, const string& token );
	
private:
	map<string, string> m_config;
	string m_path;
	string m_filename;
};

#endif // LSCONFIG_H
