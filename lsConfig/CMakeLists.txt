project(lsConfig LANGUAGES CXX)

include_directories(
)

file(GLOB MySources
	*.cpp
)
file(GLOB MyHeaders
	*.h
)

add_library(${PROJECT_NAME} STATIC
    ${MySources}
)

# install
install(TARGETS ${PROJECT_NAME} DESTINATION "lib")
install(FILES ${MyHeaders} DESTINATION "include/lsConfig")

