#include "lsConfig.h"

lsConfig::lsConfig()
{
}

bool lsConfig::load( const string& full_path )
{
	// Clear old values.
	m_config.clear();
	
	m_filename = filesystem::path( full_path ).filename();
	m_path = filesystem::path( full_path ).remove_filename();
	
	// debug
	// cout << "Path     : " << m_path << endl;
	// cout << "Filename : " << m_filename << endl;
	
	ifstream in( m_path + m_filename );
	if( in.is_open() )
	{
		cout << "Loading config file : " << m_path << m_filename << endl;
		string line;
		while( getline(in, line) )
		{
			if( line.empty() ) continue;
			
			vector<string> line_pairs = split( line, "=" );
			if( line_pairs.size() == 2 )
			{
				// Add key : value
				m_config[ line_pairs[0] ] = line_pairs[1];
			}
		}
		in.close();
		return true;
		
	} else
	{
		cerr << "ERROR: Could not open config file : " << m_path << m_filename << endl;
	}
	return false;
}

void lsConfig::save()
{
	if( m_filename.empty() )
	{
		cerr << "ERROR: No filename set." << endl;
		return;
	}
	
	cout << "Saving config : " << m_path << m_filename << endl;
	
	// Make sure our folders exists.
	if(! m_path.empty() )
	{
		if(! filesystem::exists( m_path ) )
		{
			cout << "Creating folder : " << m_path << endl;
			filesystem::create_directories( m_path );
		}
	}
	
	ofstream out( m_path + m_filename );
	if( out.is_open() )
	{
		// header
		out << "# lsConfig v0.1\n";
		for( const auto& entry : m_config )
		{
			out << entry.first << "=" << entry.second << "\n";
		}
		out.close();
		
	} else
	{
		cerr << "ERROR: Could not open config file : " << m_path << m_filename << endl;
	}
}


void lsConfig::add_entry( const string& key, const string& value )
{
	m_config[ key ] = value;
}

string lsConfig::get_value( const string& key )
{
	return m_config[ key ];
}

void lsConfig::print()
{
	cout << "Key : Value" << endl;
    for( const auto& entry : m_config )
	{
        cout << entry.first << " : " << entry.second << endl;
    }
}

// Utilities

vector<string> lsConfig::split( string str, const string& token )
{
    vector<string>result;
	
	if( str.empty() )
	{
		cerr << "ERROR: lsAgents::split : empty string" << endl;
		return result;
	}
	
    while( str.size() )
	{
        ulong index = str.find( token );
        if( index != string::npos )
		{
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if( str.size() == 0 )
			{
				result.push_back( str );
			}
        } else
		{
            result.push_back( str );
            str = "";
        }
    }
	
    return result;
}
