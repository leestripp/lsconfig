## lsConfig v0.1-Alpha
Simple config file load/save C++ library.

## Features
* load and save config files.
* stores key : value pairs.
* split() function to split the value string.

# TODO
* load and save encrypted config file.
